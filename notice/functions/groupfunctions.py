from notice.database.group import Group,getGroups
from notice.database.bridge import groupBridge
from flask import request, make_response,jsonify


#create group
def create_group(name,creator):
    group = Group(name=name,email=creator)
    group.create()
    del(group)
    return True

#delete group
def delete_group(id):
    group = Group(id=id).delete()
    del(group)
    return True

#update group name
def update_group(id,name):
    group = Group(id=id).update(new_name=name)
    del(group)
    return True

#group members
def get_groups():
    email = request.args.get('email')
    rows = getGroups(email)

    resp = make_response(jsonify(rows))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp,200

