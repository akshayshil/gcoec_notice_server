from notice.database.config import config
import mysql.connector

dbconfig = config

def getAll(statement,args):
    statement = statement
    args = args

    try:
        conn = mysql.connector.connect(**dbconfig)
        cursor = conn.cursor(buffered=True, dictionary=True)
        cursor.execute(statement, args)
        conn.commit()
        row = cursor.fetchall()
    except mysql.connector.Error as err:
        # Log the error
        print(err)

    else:
        cursor.close()
        conn.close()
        if row is None:
            return
        return row

def getOne(statement,args):
    statement = statement
    args = args

    try:
        conn = mysql.connector.connect(**dbconfig)
        cursor = conn.cursor(buffered=True,dictionary=True)
        cursor.execute(statement, args)
        conn.commit()
        row = cursor.fetchone()
    except mysql.connector.Error as err:
        # Log the error
        print(err)

    else:
        cursor.close()
        conn.close()
        if row is None:
            return
        return row

def getNumber(statement,args,number):
    statement = statement
    args = args
    number = number

    try:
        conn = mysql.connector.connect(**dbconfig)
        cursor = conn.cursor(buffered=True, dictionary=True)
        cursor.execute(statement, args)
        conn.commit()
        row = cursor.fetchmany(size=number)
    except mysql.connector.Error as err:
        # Log the error
        print(err)

    else:
        cursor.close()
        conn.close()
        if row is None:
            return
        return row