from notice.database.user import User
from notice.database.student import Student
from notice.functions.dbgetfunctions import *
import time

from flask import request, make_response, jsonify

def add_teacher():
    name = request.form['name']
    email = request.form['email']
    password = request.form['password']
    role = request.form['role']

    try:
        user = User(email=email,name=name,password=password,role=role).add()
    except:
        del(user)
        print("Could not add")
        resp = make_response("Could not add")
        resp.headers['Access-Control-Allow-Origin'] = '*'
        return resp,200
    finally:
        del(user)
    resp = make_response("Success")
    resp.headers['Access-Control-Allow-Origin'] = "*"
    return resp, 200

def delete_teacher():
    email = request.form['email']

    try:
        user = User(email=email).delete()
    except:
        print("Could not delete")
        resp = make_response("Could not Delete")
        resp.headers['Access-Control-Allow-Origin'] = '*'
        return resp,200
    finally:
        pass

def add_student():
    name = request.form['name']
    email = request.form['email']
    password = request.form['password']
    role = request.form['role']
    pnr = request.form['pnr']
    roll = request.form['roll']
    batch = request.form['batch']

    user = User(email=email,name=name,password=password)
    user.add()
    time.sleep(1)
    student = Student(email,roll,pnr,batch)
    student.add()

    resp = make_response("Success")
    resp.headers['Access-Control-Allow-Origin'] = "*"
    return resp, 200

def user_login():
    email = request.form['email']
    password = request.form['password']


def get_student_list():
    statement = (
        "SELECT u.email, u.name, p.pnr, p.roll, p.batch FROM `users` `u` INNER JOIN `student_info` `p` ON (`u`.`email` = `p`.`email`)"
    )

    row = getAll(statement,args=None)
    resp = make_response(jsonify(row))
    resp.headers['Access-Control-Allow-Origin'] = "*"
    return resp,200

def verifyLogin():
    email = request.form['email']
    password = request.form['password']
    rowCounts = is_exist_user(email,password)
    if rowCounts > 0:
        usertype = accountType(email)
        user = {
            "email": str(email),
            "usertype":usertype
        }
        resp = make_response(jsonify(user))
        resp.headers['Access-Control-Allow-Origin'] = "*"
        return resp,200
    else:
        resp = make_response("Credentials don't match")
        resp.headers['Access-Control-Allow-Origin'] = '*'
        return resp,200

def accountType(email):
    statement = (
        "SELECT `role` FROM `users` WHERE `email` = %s"
    )
    args = (email,)
    try:
        conn = mysql.connector.connect(**dbconfig)
        cursor = conn.cursor(buffered=True,dictionary=True)
        cursor.execute(statement,args)
        conn.commit()
        userType = cursor.fetchone()
        return userType['role']

    except mysql.connector.Error as err:
        print(err)
        return

def is_exist_user(email,password):
    statement = (
        "SELECT `email`, `password` FROM `users` WHERE `email` = %s AND `password` = %s"
    )
    args = (
        email,password
    )

    try:
        conn = mysql.connector.connect(**dbconfig)
        cursor = conn.cursor(buffered=True,dictionary=True)
        cursor.execute(statement,args)
        conn.commit()
        rowCount = len(cursor.fetchall())
        return rowCount

    except mysql.connector.Error as err:
        print(err)
        return