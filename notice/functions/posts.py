from notice.functions.dbgetfunctions import *
from notice.database.post import Post, getAdminNotice
from flask import request, make_response,jsonify

#POST based on users subscribed groups
def user_subscribed_posts(user_email):
    pass

#POSTs based on group
def group_posts(group_id):
    statement = "SELECT * FROM `posts`" \
                "WHERE `posts`.`group` = %s"
    args = (group_id,)
    row = getAll(statement,args)
    print(row)

#Admin posts
def admin_posts():
    return group_posts(1)

#Save post
def save_post(content,creator_email,group_id):
    try:
        post = Post(content,creator_email,group_id).create()
        del(post)
        return True
    except:
        return False

#saved post
def saved_post(user_email):
    statement = "SELECT p.*, u.email\
                FROM posts p\
                INNER JOIN saved_post s ON p.id = s.post_id\
                INNER JOIN users u ON s.user_email = u.email\
                WHERE u.email = %s;"
    args = (user_email,)
    row = getNumber(statement,args,100)
    return row

def receive_post():
    user_email = request.form['user_email']
    content = request.form['content']
    group = request.form['group']

    isSaved = save_post(user_email,content,group)
    args = "Unsuccessful"
    if isSaved is True:
        args = "Successful"

    resp = make_response(args)
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp

def create_post():
    content = request.form['content']
    email = request.form['email']
    group = request.form['group']
    t = save_post(content,email,group)
    if t == True:
        resp = make_response("Post Created")
        resp.headers['Access-Control-Allow-Origin'] = '*'
        return resp,200
    resp = make_response("Failed")
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp,200

def get_notice():
    result = getAdminNotice()
    resp = make_response(jsonify(result))
    resp.headers['Access-Control-Allow-origin'] = '*'
    return resp


