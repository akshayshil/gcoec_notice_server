import mysql.connector
from notice.database.config import config

dbconfig = config

class Post:
    def __init__(self,content,author,group,id=None):
        self.id = None
        self.content = content
        self.author = author
        self.group = group

    def create(self):
        statement = (
            "INSERT INTO `software_engg`.`posts`"
            "(`content`,`author`,`group`)"
            "VALUES (%s,%s,%s);"

        )

        args = (self.content, self.author,self.group)

        try:
            conn = mysql.connector.connect(**dbconfig)
            cursor = conn.cursor()
            cursor.execute(statement,args)
            conn.commit()
        except mysql.connector.Error as err:
            #Log the error
            print(err)

        else:
            cursor.close()
            conn.close()

    def delete(self,id):
        statement = (
            "DELETE FROM `software_engg`.`posts`"
            "WHERE `id` = %s"
        )

        args = (id,)

        try:
            conn = mysql.connector.connect(**dbconfig)
            cursor = conn.cursor()
            cursor.execute(statement,args)
            conn.commit()
        except mysql.connector.Error as err:
            #Log the error
            print(err)

        else:
            cursor.close()
            conn.close()

    def update(self,id):
        statement = (
            "UPDATE `posts` SET"
            "`content` = %s,"
            "WHERE `posts`.`id` = %s;"
        )

        args = (self.content,self.author,id)
        try:
            conn = mysql.connector.connect(**dbconfig)
            cursor = conn.cursor()
            cursor.execute(statement,args)
            conn.commit()
        except mysql.connector.Error as err:
            #Log the error
            print(err)

        else:
            cursor.close()
            conn.close()

    #TODO:- Query posts
    def query(self):
        pass

def getAdminNotice():
    statement = "select `posts`.* from `posts` where `posts`.`group` = %s"
    args = (1,)
    try:
        conn = mysql.connector.connect(**dbconfig)
        cursor = conn.cursor(buffered=True,dictionary=True)
        cursor.execute(statement,args)
        conn.commit()
        row = cursor.fetchall()
    except mysql.connector.Error as err:
        print(err)
    else:
        cursor.close()
        conn.close()
    return row