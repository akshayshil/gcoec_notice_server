import mysql.connector
from notice.database.config import config as dbconfig

config = dbconfig

class PostBridge:

    def add(self,post_id,user_email):
        statement = (
            "INSERT INTO `software_engg`.`saved_post`"
            "VALUES (%s, %s);"
        )

        args = ( post_id, user_email )

        try:
            conn = mysql.connector.connect(**dbconfig)
            cursor = conn.cursor()
            cursor.execute(statement,args)
            conn.commit()
        except mysql.connector.Error as err:
            #Log the error
            print(err)

        else:
            cursor.close()
            conn.close()

    def delete(self, post_id, user_email):
        statement = (
            "DELETE FROM `software_engg`.`saved_post` WHERE post_id = %s AND user_email = %s;"
        )

        args = (post_id, user_email)

        try:
            conn = mysql.connector.connect(**dbconfig)
            cursor = conn.cursor()
            cursor.execute(statement,args)
            conn.commit()
        except mysql.connector.Error as err:
            #Log the error
            print(err)

        else:
            cursor.close()
            conn.close()