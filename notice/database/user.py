"""
User table CRUD Operations
"""

import mysql.connector
from notice.database.config import config

dbconfig = config

class User:

    def __init__(self,email,name = None,password = None,role=None):
        self.email = email
        self.name = name
        self.password = password
        self.role = role

    def __repr__(self):
        return "Email: %s, Name: %s, Password: %s, Role: %s"%(self.email, self.name, self.password, self.role)

    def add(self):
        #Insert the basic info
        statement = ("INSERT INTO users"
                     "(name,email,password,role)"
                    "VALUES (%s,%s,%s,%s)"
                     )
        args = (self.name,self.email,self.password,self.role)

        try:
            conn = mysql.connector.connect(**dbconfig)
            cursor = conn.cursor()
            cursor.execute(statement,args)
            print("Executed")
            conn.commit()
        except mysql.connector.Error as err:
            #Log the error
            print(err)

        else:
            cursor.close()
            conn.close()


    def delete(self):
        statement = (
            "DELETE FROM users WHERE email=%s;"
        )

        args = (self.email,)

        try:
            conn = mysql.connector.connect(**dbconfig)
            cursor = conn.cursor()
            cursor.execute(statement,args)
            conn.commit()
        except mysql.connector.Error as err:
            print(err)

        else:
            cursor.close()
            conn.close()

    def update(self,email,name,password,role):
        statement = (
            "update user set "
            "email = %s, name = %s, password = %s, role = %s"
            "where email = %s;"
        )

        args = (email,name,password,role)

        try:
            conn = mysql.connector.connect(statement,args)
            cursor = conn.cursor()
            cursor.execute(statement,args)
            conn.commit()
        except mysql.connector.Error as err:
            print(err)
        else:
            cursor.close()
            conn.close()

    def query(self):
        statement = (
            "SELECT `email`, `password` FROM `users` WHERE `email` = %s and `password` = %s"
        )

        args = (self.email,self.password)
        try:
            conn = mysql.connector.connect(statement,args)
            cursor = conn.cursor(buffered=True,dictionary=True)
            cursor.execute(statement,args)
            conn.close()
            row = cursor.rowcount
            if row > 0:
                return True
            else:
                return False
        except mysql.connector.Error as err:
            print(err)
            return

    def accountType(self):
        statement = (
            'SELECT `role` FROM `users` WHERE `email` = %s'
        )

        args = (self.email,)
        try:
            conn = mysql.connector.connect(statement,args)
            cursor = conn.cursor(buffered=True,dictionary=True)
            cursor.execute(statement,args)
            conn.commit()
            row = cursor.fetchone()

        except mysql.connector.Error as err:
            print(err)
        else:
            cursor.close()
            conn.close()
            return row

    #TODO:- User's group
    #TODO:- Users's saved posts

