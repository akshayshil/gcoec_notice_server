import mysql.connector
from notice.database.config import config

dbconfig = config

class groupBridge:
    def __init__(self, user_email = None,group_id = None):
        self.user_email = user_email
        self.group_id = group_id

    def set_user_email(self,user_email):
        self.user_email = user_email

    def set_group_id(self,group_id):
        self.group_id = group_id

    def add(self):
        statement = (
            "INSERT INTO `software_engg`.`groups_user`(`group_id`,`user_email`) VALUES\
            (%s, %s);"
        )

        args = (self.group_id,self.user_email)
        try:
            con = mysql.connector.connect(**dbconfig)
            cur = con.cursor()
            cur.execute(statement,args)
            con.commit()
        except mysql.connector.Error as err:
            print(err)
        else:
            cur.close()
            con.close()

    def remove(self):
        statement = (
            "DELETE FROM `software_engg`.`groups_user` WHERE `user_email` = %s AND `group_id` = %s"
        )

        args = (self.user_email,self.group_id)

        try:
            con = mysql.connector.connect(**dbconfig)
            cur = con.cursor()
            cur.execute(statement,args)
            con.commit()
        except mysql.connector.Error as err:
            print(err)
        else:
            cur.close()
            con.close()

    def members(self):
        statement = (
            "SELECT `users`.`name`, `users`.`email`\
            FROM `users`,`groups_user`\
            INNER JOIN `users` AS `u` ON ( `u`.`email` = `groups_user`.`user_email` )\
            INNER JOIN `groups` AS `gu` ON ( `gu`.`id` = `groups_user`.`group_id` )\
            WHERE `groups_user`.`group_id` =  %s;"
        )

        args = (self.group_id,)

        try:
            conn = mysql.connector.connect(**dbconfig)
            cursor = conn.cursor(buffered=True,dictionary=True)
            cursor.execute(statement,args)
            conn.commit()
            row = cursor.fetchall()

        except mysql.connector.Error as err:
            print(err)
        else:
            cursor.close()
            conn.close()
            if row is not None:
                return row