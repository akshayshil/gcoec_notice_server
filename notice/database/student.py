"""
Student table update
"""
import mysql.connector
from notice.database.config import config

dbconfig = config

class Student:
    def __init__(self, email, roll = None, pnr = None, batch = None):
        self.email = email
        self.roll = roll
        self.pnr = pnr
        self.batch = batch

    def __repr__(self):
        if self.email is None or (self.pnr and self.roll and self.batch is None):
            return "\n"
        return "Student Record:- Roll Number: %s, PNR: %s, batch: %s\n"%(self.roll,self.pnr,self.batch)

    #If parent table record doesn't exist, it will raise an error and exit
    def add(self):
        statement = (
            "INSERT INTO student_info (email, pnr, roll, batch)"
            "VALUES (%s, %s, %s, %s)"
        )
        args = (self.email,self.pnr, self.roll, self.batch)
        try:
            conn = mysql.connector.connect(**dbconfig)
            cur = conn.cursor()
            cur.execute(statement,args)
            conn.commit()
        except mysql.connector.Error as err:
            print(err)
        else:
            cur.close()
            conn.close()

    def delete(self):
        statement = (
            "DELETE FROM student_info WHERE email = %s;"
        )

        args = (str(self.email),)

        try:
            conn = mysql.connector.connect(**dbconfig)
            cur = conn.cursor()
            cur.execute(statement,args)
            conn.commit()
        except mysql.connector.Error as err:
            print(err)

        else:
            cur.close()
            conn.close()

    def query(self):
        statement = (
            "SELECT * FROM student_info WHERE email = %s;"
        )

        args = (self.email,)

        try:
            conn = mysql.connector.connect(**dbconfig)
            cursor = conn.cursor(buffered=True,dictionary=True)
            cursor.execute(statement,args)
            conn.commit()
            row = cursor.fetchone()
            if row == None:
                pass
            else:
                self.roll = row['roll']
                self.pnr = row['pnr']
                self.batch = row['batch']

        except mysql.connector.Error as err:
            print(err)
        else:
            cursor.close()
            conn.close()

    def update(self):
        pass

def addStudent(email,pnr,roll,batch):
    statement = (
        "INSERT INTO `software_engg`.`student_info` (email,pnr,roll,batch) VALUES (%s,%s,%s,%s)"
    )
    args = (email,pnr,roll,batch)
    try:
        conn = mysql.connector.connect(**dbconfig)
        cursor = conn.cursor()
        cursor.execute(statement, args)
        conn.commit()

    except mysql.connector.Error as err:
        print(err)
    else:
        cursor.close()
        conn.close()

