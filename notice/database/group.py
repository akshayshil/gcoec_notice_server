import mysql.connector
from notice.database.config import config

dbconfig = config

class Group:
    def __init__(self,name = None,id=None,email=None):
        self.name = name
        self.id = id
        self.email = email

    def set_id(self,id):
        self.id = id

    def set_email(self,email):
        self.email = email

    def set_name(self,name):
        self.name = name

    def create(self):
        statement = (
            "INSERT INTO `software_engg`.`groups`(`name`,`creator`) VALUES (%s,%s);"
        )
        args = (self.name,self.email)

        try:
            con = mysql.connector.connect(**dbconfig)
            cur = con.cursor()
            cur.execute(statement,args)
            con.commit()
        except mysql.connector.Error as err:
            print(err)
        else:
            cur.close()
            con.close()

    def delete(self):
        statement = (
            "DELETE FROM `groups` WHERE id = %s;"
        )

        args = (self.id,)

        try:
            con = mysql.connector.connect(**dbconfig)
            cur = con.cursor()
            cur.execute(statement,args)
            con.commit()
        except mysql.connector.Error as err:
            print(err)
        else:
            cur.close()
            con.close()

    def update(self, new_name):
        if self.id is None or new_name == "":
            return
        statement = (
            "UPDATE `software_engg`.`groups` SET\
            `name` = %s,\
            WHERE `id` = %s;"
        )

        args = (
            new_name, self.id
        )

        try:
            con = mysql.connector.connect(**dbconfig)
            cur = con.cursor()
            cur.execute(statement,args)
            con.commit()
        except mysql.connector.Error as err:
            print(err)
        else:
            cur.close()
            con.close()


    #TODO:- Query Members
def queryMembers(self):
    pass

def getGroups(email):
    statement = (
        "SELECT g.`id`, g.`name` FROM `groups` AS g WHERE g.creator = %s"
    )
    args = (email,)
    try:
        con = mysql.connector.connect(**dbconfig)
        cur = con.cursor(buffered=True, dictionary=True)
        cur.execute(statement, args)
        con.commit()
        row = cur.fetchall()
    except mysql.connector.Error as err:
        print(err)
    else:
        cur.close()
        con.close()
        return row