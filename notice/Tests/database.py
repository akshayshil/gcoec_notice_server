import mysql.connector

#Configuration for database
dbConfig = {
    "user":"root",
    "password":"",
    "database":"software_engg",
    "host":"localhost"
}

#Test connection
def checkConnection():
    try:
        cnx = mysql.connector.connect(**dbConfig)
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("Username or Password is wrong")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("Database does not exist")
        else:
            print(err)

    else:
        cnx.close()


