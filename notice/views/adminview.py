from flask import make_response, request, Request
from notice.functions.userfunc import *
from notice.functions.posts import *
from notice.functions.groupfunctions import *
from notice import app

#TextEditor

@app.route('/api/admin/home/')
def adminHome():
    return "Admin Home"

@app.route('/api/save/post', methods=['POST'])
def savePost():
    return receive_post()

@app.route('/api/register/staff',methods=['POST'])
def addTeacher():
    return add_teacher()

@app.route('/api/teacher/get_groups')
def getGroups():
    return get_groups()

@app.route('/api/editor',methods=['POST'])
def createPost():
    return create_post()

@app.route('/api/studentlist')
def getStudentList():
    return get_student_list()

@app.route('/api/get_notice')
def getNotice():
    return get_notice()

@app.route('/api/login',methods=['POST'])
def login():
    return verifyLogin()