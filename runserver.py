"""
This script runs the Noticeapp application using a development server.
"""

from notice import app

if __name__ == '__main__':
    try:
        app.run(debug=True)
    except:
        print("Could not run")